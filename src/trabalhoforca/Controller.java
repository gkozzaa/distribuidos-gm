/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhoforca;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Marcelo
 */
public class Controller extends Thread {

    private String apelido;
    private String ip;
    private Integer id;
    public ArrayList<String> Jogadores = new ArrayList<>();
    public ArrayList<Integer> Ids = new ArrayList<>();
    public int maximoJogadores = 4;

    public Controller(String apelido, String ip) {
        this.apelido = apelido;
        this.ip = ip;
        this.setId();
    }

    public void setId() {

        Random random = new Random();
        this.id = random.nextInt(10);

//        this.Jogadores.add(nomeID);
//        this.Ids.add(id);
//        startClient(id);
    }

    /*public void setJogadores(String a) {
        System.out.println("ENTROU AQUI CARALHO");
        int numCaracter = a.indexOf("#");
        String apelido = a.substring(0, numCaracter);
        String id = a.substring(numCaracter + 1, a.length());
        System.out.println(a);
        Jogadores.add(apelido);
        Ids.add(Integer.parseInt(id));

    }
     */
    private void setJogadores(String str) {

        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == '#') {
                Jogadores.add(str.substring(0, i));
                Ids.add(Integer.parseInt(str.substring(i + 1, i + 2)));
                break;
            }
        }
    }

    public void startClient() {

        Client client = new Client(this);
        client.start();

    }

    public boolean verificaApelido(DatagramPacket d) {
        //método responsável por verificar se já está cadastrado algum apelido
        String str = new String(d.getData());
        str = str.substring(0, d.getLength());
        //[apelido,id]
        int i = str.indexOf("#");
        if (i > 0) {
            str = str.substring(0, i);//apelido somente
        }

        if (str.equals(apelido)) {
            return true;
        }
        for (int j = 0; j < maximoJogadores; j++) {
            if (Jogadores != null) {

                if (Jogadores.contains(str)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void escolheGerador() {
        //escolhe o menor ID para ser o gerador da palavra 
        int idMenor = id;

        for (int i = 0; i < this.Ids.size(); i++) {
            if (Ids.get(i) < idMenor) {
                idMenor = Ids.get(i);
            }
        }

        if (idMenor == id) {
            System.out.println("Sou o gerador");
            startServer();
        } else {
            System.out.println("Sou Jogador");
            startJogador();
        }
    }

    public void startServer() {
        Servidor server = new Servidor();
        Thread servidor = new Thread(server);
        servidor.start();
    }

    public void startJogador() {
        Thread j = new Thread(this);
        j.start();

    }

    public void run() {
        String nomeID = this.apelido + "#" + this.id;
        InetAddress group = null;
        MulticastSocket s = null;
        try {
            group = InetAddress.getByName("228.5.6.7");
            s = new MulticastSocket(6789);
            s.joinGroup(group);

            byte[] m = nomeID.getBytes();
            DatagramPacket ID = new DatagramPacket(m, m.length, group, 6789);

            int count = 0;
            while (count < maximoJogadores) {
//                System.out.println(ID);

                s.send(ID);

                byte[] buffer = new byte[1000];
                DatagramPacket messageIn = new DatagramPacket(buffer, buffer.length);
                //laço para evitar ficar enviando dados desnecessariamente

                do {

                    s.receive(messageIn); //se passar quer dizer que recebeu MSG   

                } while (new String(messageIn.getData()).equals(this.apelido + "#" + this.id) || verificaApelido(messageIn));

                if (!verificaApelido(messageIn)) {
                    String aux = new String(messageIn.getData());
                    aux = aux.substring(0, messageIn.getLength());
                    if (count != 0) {
                        //armazena somente os outros integrantess
                        setJogadores(aux);
                    }

                    count++;
                }

            }
            
            s.send(ID);
//            s.send(ID);
            System.out.println(Jogadores.size());
            if(Jogadores.size() == maximoJogadores - 1){
                escolheGerador();
            }
            startClient();
           
            

        } catch (UnknownHostException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
