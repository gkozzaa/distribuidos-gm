/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalhoforca;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Marcelo
 */
public class Client extends Thread{
    int id;
    Controller c;
    public Client(Controller c){
        this.c = c;
        getUsers();
        
    }
    
    
    public void getUsers(){
     
        
        for(int i =0; i <c.Jogadores.size();i++){
            System.out.println("Nome dos demais Jogadores: " + c.Jogadores.get(i)+"#"+c.Ids.get(i));
        }
    }
    
    @Override
    public void run() {
        System.out.println("Entrei aqui");
        MulticastSocket s = null;
        InetAddress group;
          
        try {
            group = InetAddress.getByName("228.5.6.7");

            s = new MulticastSocket(6789);
            s.joinGroup(group);
 
          while(true){
               String mensagem;
               mensagem = JOptionPane.showInputDialog("Digite a mensagem:");
               byte[] m = mensagem.getBytes();
                DatagramPacket messageOut = new DatagramPacket(m, m.length, group, 6789);
               s.send(messageOut);
               
          }
            
        } catch (UnknownHostException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
}
